// Global array of all cells
var cells;
// Global timer controlling game tick speed in ms
var tickSpeed = 100;
// Global initial cell count
// TODO: don't hardcode this
cellCount = 4900;


// Main setup function
var mainInitialize = function() {
  initializeCells(cellCount);
  renderCells();
};

// Main game loop
var mainUpdate = function() {
  // Do stuff
  updateCells();
  renderCells();
};


// Initializes global cells array
var initializeCells = function(numberOfCells) {
  cells = [];
  for (i = 0; i < numberOfCells; i++) {
    cells[i] = generateCell(i);
  }
};

// Returns a cell object
var generateCell = function(id) {
  var cell = {
    id: id,
    state: getRandomInt(0, 4)
  };
  return cell;
};

// Updates global cells array with a new state every tick
var updateCells = function() {
  for (i = 0; i < cells.length; i++) {
    cells[i].state = getRandomInt(0, 4);
  }
};

// Writes array of cells to html
var renderCells = function() {
  var cellsHtml = "";
  for (i = 0; i < cells.length; i++) {
    cellsHtml += '<div class="cell state-' + cells[i].state + '" id="cell-' + cells[i].id + '"></div>';
  }
  $("#cell-container").empty();
  $("#cell-container").append(cellsHtml);
};

// Run on page load
$(document).ready(function() {
  mainInitialize();
  setInterval(mainUpdate, tickSpeed);
});




// Helper functions
//The maximum is exclusive and the minimum is inclusive
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}
